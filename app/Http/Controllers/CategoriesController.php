<?php

namespace App\Http\Controllers;
use App\Categories;
use Illuminate\Http\Request;
use DB;
 
class CategoriesController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(Categories $users)
    {
        $key=false;
    	$categories = Categories::all();
        return view('categories/index', ['users' => $users->paginate(10), 'categories'=> $categories,'id'=>'All','key'=>$key]);
    }

    /**
     * Show the form for creating a new user
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('categories/create');
    }

    /**
     * Store a newly created user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
    	if($request->name != '' && $request->image != '' &&$request->publish != '' )
    	{
	        $users = new Categories();
	    	$users->name = $request->name ;
	    	$users->slug = $request->slug ;
	    	$users->image= $request->image ;
	    	$users->publish = $request->publish ? 1 : 0 ;
	    	$users->updated_at = now();
	    	$users->save();
	       	$request->session()->flash('success', 'Bài viết được tạo thành công!');
       		return redirect()->route('indexCategories');
        }
        else
    	{
            $id=$request->getid;
    		$request->session()->flash('fail', 'Hãy điền đầy đủ thông tin!');
       		return view('categories/create',['id'=>$id]);
    	}
    }
	    public function slug($str) {
	    $string=Str::slug($str, '-');
	    return $string;
		}
    /**
     * Show the form for editing the specified user
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $id = $request->id ;
        $user = Categories::find($id);
    	return view('categories/edit',['id'=>$id,'user'=>$user]);
    }

    /**
     * Update the specified user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
    	$users = Categories::find($request->getid);
    	if($request->name != '' && $request->image != '' &&$request->publish != '' )
    	{
	    	$users = Categories::find($request->getid);
	    	$users->name  = $request->name;  
	    	$users->image = $request->image; 
            $users->slug = $request->slug ;
	    	$users->publish =  $request->publish ? 1 : 0; 
	    	$users->updated_at = now();
	    	$users->save();
	    	$request->session()->flash('success', 'Bài viết được update thành công!');
       		return redirect()->route('indexCategories');
    	}
    	else
    	{
            $id=$request->getid;
    		$request->session()->flash('fail', 'Hãy điền đầy đủ thông tin!');
       		return view('categories/edit',['id'=>$id,'user'=>$users]);
    	}
    }

    /**
     * Remove the specified user from storage
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {

    	$id = $request->takeid;
    	if(is_array($id))
    	{
    		foreach ($id as $row) 
    	 	{
	            $users = Categories::findOrFail($row);
	        	$users->delete();
        	}
    	}
    	else
    	{
    		 $users = Categories::findOrFail($id);
    		 $users->delete();
    	}
        $request->session()->flash('delete', 'Bài viết được xóa thành công!');
    	return redirect()->route('indexCategories');
    }

	public function filter(Request $request)
	{
        $key=true;
		$categories = Categories::all();
		$timeValue = explode('-', $request->date);
            $start = date('Y-m-d', strtotime($timeValue[0]));
            $end = date('Y-m-d', strtotime($timeValue[1]));
            $requests = array('name'=> $request->name,'updated_at' => $start);
    	foreach ($requests as $key => $value) 
    	{
    		if($value != 'All')
    			{
		    		if($key == 'updated_at' && $start != $end)
		    		{
		    			$DB[] = array($key,'>=',$value);
		    			$DB[]=array($key,'<=',$end);   
		    		}
		    		elseif($key != 'updated_at' && $value != null)
					{
					    $DB[] = array($key, '=', $value); 
					}
		    	}
    	}
        if(isset($DB))
        {
            $user = Categories::where($DB)->paginate(8); 
            if($user[0]==null)
            {
            	$categories = Categories::all();
        		return view('categories/index', ['users' => $user, 'categories'=> $categories,'id'=>'All','key'=>$key]);
            }
            else
            {
            	if($request->name=='All')
            	{
            		return view('categories/index',['users'=>$user,'categories'=> $categories,'id'=>'All','key'=>$key]);
            	}
            }
        }  
    	else
        {
            return redirect()->route('indexCategories');
        }
        return view('categories/index',['users'=>$user,'categories'=> $categories,'id'=>$user[0]->name,'key'=>$key]);
	}
	public function activate(Request $request)
	{
		if($request->checkbox == null)
		{
			$request->session()->flash('fail', 'Xin mời bạn hãy chọn bất kì 1 ô nào đó !!! ');
       		return redirect()->route('indexCategories');
		}
        $id=$request->checkbox;
			if (is_array($id)) 
            {
            foreach ($id as $item) 
            {
                $list = Categories::findOrfail($item);
                if($list->publish) 
                {
                    $list->publish = false;
                    $list->save();
                } else {
                    $list->publish = true;
                    $list->save();
                }
            }
        } 
        else 
        {
            $list = Categories::findOrfail($id);
            if($list->publish) 
            {
                $list->publish = false;
                $list->save();
            } else 
            {
                $list->publish = true;
                $list->save();
            }
        }
        $request->session()->flash('success', 'Kích hoạt / Vô hiệu hóa thành công !!!');
        return redirect()->route('indexCategories'); 
    }
}



