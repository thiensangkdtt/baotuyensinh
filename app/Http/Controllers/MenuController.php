<?php

namespace App\Http\Controllers;
use Harimayco\Menu\Models\Menus;
use Harimayco\Menu\Models\MenuItems;
use Illuminate\Http\Request;
use Harimayco\Menu\Facades\Menu;

class MenuController extends Controller
{
    public function index()
    {
    	// $public_menu = Menu::get(35);
        return view('menu/index');
    }
}
