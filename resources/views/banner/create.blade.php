@extends('layouts.app', ['pageSlug' => 'banner'])

@section('content')
 <script src="/ckfinder/ckfinder.js"></script>     
 @if (session('status'))
        <div class="alert alert-danger">{{session('status')}}</div>
 @endif 
 {{ Breadcrumbs::render('createBanner') }}
 <form action="{{ route('storeBanner') }}" method="post">
  @csrf
<div style="background-color: #f7f2dd;" class="row">
  <div class="card-body col-lg-9">
	    <div class="form-group">
        <label style="color: black;" for="exampleInputEmail1">Tên Banner</label>
        <input style="color: black" type="textbox" required class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tên Banner" name="name">
        
      </div>
      <div class="form-group">
        <label style="color: black;" for="exampleInputEmail1">url</label>
        <input style="color: black" required type="textbox" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="url" name="link">       
      </div>

      <div class="form-group">
        <label style="color: black;" for="exampleInputEmail1">Nội Dung Banner </label>
        <textarea required style="color: black;" class="form-control" id="editor" aria-describedby="emailHelp" name="description" placeholder="Nội dung Banner"></textarea>
        
      </div>
     <div  style="background-color: #f7f2dd;" class="card">
        <div class="card-body">
              <div class="form-group">
                <label style="color: black; margin-right: 10px;" for="exampleFormControlSelect2">Vị Trí</label>
                <select style="padding: 10px 10px; border-radius: 3px;" data-toggle="dropdown" style="color: black;"  id="exampleFormControlSelect2" name ="position">
                    <option class="dropdown-item" value="Top">Top</option>
                    <option class="dropdown-item" value="Banner">Banner</option>
                    <option class="dropdown-item" value="Sidebar">Sidebar</option>
                    <option class="dropdown-item" value="Sidebar_cate">Sidebar_cate</option>
                </select>
              </div>
          </div>
      </div>
          <label style="color: black;" for="exampleInputEmail1">Publish</label>
            <div style="margin-left: 30px">
              <input class="form-check-input" type="checkbox" name="publish" id="inlineRadio1" checked>
              <label class="form-check-label" for="inlineRadio1" style="color: black;margin-right: 30px">Đăng bài</label>
            </div>
<div style=" margin-top: 20px;">
          <button type="submit" style="color:white; background-color: #4caf50; padding: 10px 12px; border: 0px">Submit</button>
        </div>
    
  </div>
  <div style="padding-top: 23px;" class="col-lg-3">
    <div>
        <label style="color: black;" for="exampleFormControlSelect2">Hình Ảnh</label>
        <input class="form-control" style="color: black; margin-right: 20px; max-width: 50%;display: none;" type="text" size="48" name="image" id="url">
        <button style="margin-top: 10px; color: white; border: 0px; background-color: #4caf50; padding: 9px 15px;" type="button" onclick="openPopup()">Select file</button>
        <div style="margin-top: 15px; height: 200px; width: 200px;">
          <img class="img-fluid" id="photo" name="image" src="/laravel5/uploads/images/demoTuyenSinh/no-image.jpg" alt="">
        </div>
    </div>
  </div>
</div>
</form>
<script>
         function openPopup() {
             CKFinder.popup( {
                 chooseFiles: true,
                 onInit: function( finder ) {
                     finder.on( 'files:choose', function( evt ) {
                         var file = evt.data.files.first();
                         document.getElementById( 'photo' ).src = file.getUrl();
                     } );
                     finder.on( 'file:choose:resizedImage', function( evt ) {
                         document.getElementById( 'photo' ).src = evt.data.resizedUrl;
                     } );
                     finder.on( 'files:choose', function( evt ) {
                         var file = evt.data.files.first();
                         document.getElementById( 'url' ).value = file.getUrl();
                     } );
                     finder.on( 'file:choose:resizedImage', function( evt ) {
                         document.getElementById( 'url' ).value = evt.data.resizedUrl;
                     } );
                 }
             } );
         }
</script>
@endsection
