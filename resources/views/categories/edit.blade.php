@extends('layouts.app', ['pageSlug' => 'categories'])

@section('content')
  <script src="/ckfinder/ckfinder.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
 <meta name="csrf_token" content="{{ csrf_token() }}" />
 <script>$.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')} })</script>
  @if (session('fail'))
        <div class="alert alert-danger">{{session('fail')}}</div>
 @endif 
  {{ Breadcrumbs::render('createCategories') }}
  <form action="{{ route('storeCategories') }}" method="post">
<div style="background-color: #f7f2dd;" class="row">
  <div class="col-lg-9">
      @csrf
       <div class="form-group">
            <label style="color: black" for="exampleInputEmail1">Mục Tin</label>
            <input required style="color: black" type="textbox" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Tên Mục Tin" name="name" value="{{ $user->name }}">
        </div>
        <div class="form-group">
          <label style="color: black;" for="exampleInputEmail1">Slug</label>
          <input required style="color: black;" type="textbox" class="form-control" id="slug" placeholder="slug" name="slug"value="{{ $user->slug }}">
        </div>
        <label style="color: black;" for="exampleInputEmail1">Publish</label>
        <div style="margin-left: 30px">
          <input class="form-check-input" type="checkbox" name="publish" id="inlineRadio1" checked>
          <label class="form-check-label" for="inlineRadio1" style="color: black;margin-right: 30px">Đăng bài</label>
        </div>
        <button style="color:white; background-color: #4caf50; padding: 10px 10px; border: 0px;max-width: 10%" type="submit">Submit</button>
  </div>

  
  <div class="col-lg-3">
    <label style="color: black;" for="exampleFormControlSelect2">Hình Ảnh</label>
      <input required class="form-control" style="color: black; margin-right: 20px;display: none;" type="text" size="48" name="image" id="url" value="{{ $user->image }}">
        <button style="color: white; border: 0px; background-color: #4caf50; padding: 9px 15px;" type="button" onclick="openPopup()">Select file</button>
      <div style="margin-top: 15px; height: 200px; width: 200px;">
          <img class="img-fluid" id="photo" name="image" src="{{ $user->image }}" alt="">
      </div>
  </div>
</div>
</form>
<script>
         function openPopup() {
             CKFinder.popup( {
                 chooseFiles: true,
                 onInit: function( finder ) {
                     finder.on( 'files:choose', function( evt ) {
                         var file = evt.data.files.first();
                         document.getElementById( 'photo' ).src = file.getUrl();
                     } );
                     finder.on( 'file:choose:resizedImage', function( evt ) {
                         document.getElementById( 'photo' ).src = evt.data.resizedUrl;
                     } );
                     finder.on( 'files:choose', function( evt ) {
                         var file = evt.data.files.first();
                         document.getElementById( 'url' ).value = file.getUrl();
                     } );
                     finder.on( 'file:choose:resizedImage', function( evt ) {
                         document.getElementById( 'url' ).value = evt.data.resizedUrl;
                     } );
                 }
             } );
         }
</script>
@endsection
