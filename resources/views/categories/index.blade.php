@extends('layouts.app', ['pageSlug' => 'categories'])

@section('content')
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>  
        <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
         <script >
            $(document).ready(function(){
                $('.check').click(function(){
                    var id = new Array();
                    $('input:checkbox:checked').each(function() 
                    {
                        if(this.checked){
                            id.push($(this).val());
                        }
                    });
                    $('#destroy').click(function(){
                        Swal.fire({
                          title: 'Are you sure?',
                          text: "You won't be able to revert this!",
                          type: 'warning',
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Yes, delete it!'
                        }).then((result) => {
                            if (result.value) {
                                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                                $.post("{{ route('destroyCategories') }}", {takeid:id,_token:CSRF_TOKEN}, function (data){ 
                                    for (var i = 0; i < id.length; i++) {
                                        $('#detail_'+id[i]).fadeTo('slow',0.7,function()
                                        {
                                            $(this).remove();
                                        });
                                    }
                                     Swal.fire(
                                          'Deleted!',
                                          'Your file has been deleted.',
                                          'success'
                                    )
                            });
                        };
                    });
                })
            })
        });
        </script>
@if (session('success'))
        <div class="alert alert-success">{{session('success')}}</div>
@endif
@if (session('delete'))
        <div class="alert alert-success">{{session('delete')}}</div>
@endif
@if (session('search'))
        <div class="alert alert-danger">{{session('search')}}</div>
@endif
@if (session('fail'))
        <div class="alert alert-danger">{{session('fail')}}</div>
@endif
{{ Breadcrumbs::render('filterCategories',$key) }}
 <table class="table">
     <div style="margin-bottom: 15px;" class="row">
        <div style="padding-top: 28px; padding-right: 55px;" class="col-lg-1">
            <form action="{{ route('createCategories') }}" method="get" accept-charset="utf-8">
                <button style=" color:white; background-color: #4caf50; padding: 10px 12px; border: 0px" class=" animation-on-hover" type="submit"><i class="tim-icons icon-simple-add"> </i></button>
            </form>
        </div>
        <div class="col-lg-9">
            <form action="{{ route('filterCategories') }}" method="get" accept-charset="utf-8">
                <div class="row">
                    <div class="col-lg-5">
                        <h3 style="color: black;margin-top: 30px">Phần mục tin</h3>
                    </div>
                    <div class="form-group col-md-3">
                      <label style="color: black;" for="exampleInputEmail1">Mục Tin </label>
                            <select  style=" color: black ; width: 100%;"id="publish" class="form-control" name="name">
                                <option selected style="display: none" class="dropdown-item" value="{{ $id }}">{{ $id }}</option>
                                <option value="All">All</option>
                                @foreach($categories as $row)
                                {
                                    <option value="{{ $row->name }}">{{ $row->name }}</option>
                                }
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputState">Updated_at</label>
                        <input style=" color: black ;" id="daterange" type="text" class="form-control" name="date" />
                    </div>
                    <div style="padding-top: 28px;" class="col-md-1">
                        <button type="submit" id="search" value="Search" style=" color:white; background-color: #4caf50; padding: 10px 12px; border: 0px" class=" animation-on-hover" type="submit"><i class="tim-icons icon-zoom-split"> </i></button>
                    </div>
                </div>
            </form>
        </div>
         <div style="padding-top: 28px;" class="col-lg-2">
            <div style="padding-bottom: 10px; right: 0px; position: relative;">
                <input  style="width: 100%; color:white; background-color: #4caf50; padding: 10px 10px; border: 0px"  type="button" id="destroy" value="Xóa Bài">
            </div>
<form action="{{ route('activateCategories') }}" method="post">
    @csrf
            <div style=" right: 0px; position: relative;">
                <input style=" width: 100%; color:white; background-color: #4caf50; padding: 10px 10px; border: 0px" type="submit" id="activate" value="Kích hoạt/Vô Hiệu Hóa">
            </div>
        </div>
        
    </div>
    <thead>
        <tr>
            <th style="border: 1px solid  #c2bebb;color: white;background-color: #4caf50"><input type="checkbox" name="checkall" id="checkall"></th>
            <th style="border: 1px solid  #c2bebb;color: white;background-color: #4caf50" class="text-center">Ảnh</th>
            <th style="border: 1px solid  #c2bebb;color: white;background-color: #4caf50" class="text-center">Mục Tin</th>
            <th style="border: 1px solid  #c2bebb;color: white;background-color: #4caf50" class="text-center">Publish</th>
            <th style="border: 1px solid  #c2bebb;color: white;background-color: #4caf50" class="text-center">Update</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
           <tr id="detail_{{ $user->id }}">
            <td style="border: 1px solid  #c2bebb;"><input type="checkbox" name="checkbox[]" class="check" value="{{ $user->id }}"></td>
            <td style="width: 35%; border: 1px solid  #c2bebb;"><img style="padding-left: 20px; max-width: 70%;" class="img-fluid" src="{{ $user->image }}" alt=""></td>
            <td style="border: 1px solid  #c2bebb;"class="text-left"><a href="{{ route('editCategories') }}?id={{ $user->id }}" style="color: green">{{ $user->name }}</a></td>
            <td style="border: 1px solid  #c2bebb;"class="text-left"> 
                <span style="background-color: {{ $user->publish ? '#4caf50' : '#c41700' }}; color: white; padding: 5px 5px;">{{ $user->publish ? 'ON' : 'OFF' }}</span>
            </td>
            <td style="border: 1px solid  #c2bebb;"class="text-left">{{ $user->updated_at }}</td>
        </tr>
        @endforeach
    </tbody>
</form>
</table>
    <div id="pagination">
        <tr>{{ $users->appends($_GET)->links() }}</tr>
    </div>
<script>
    $('#checkall').change(function(){
        $(".check").prop("checked",$(this).prop("checked"))
    })
    $(".check").change(function(){
        if($(this).prop("checked")==false){
            $("#checkall").prop("checked",false)
        }
        if($(".check:checked").length==$(".check").length){
            $("#checkall").prop("checked",true)
        }
    })
</script>
<script type="text/javascript">
    $('#daterange').daterangepicker();
</script>
<script>
     $(document).ready(function(){
        $('#destroy').click(function(){
            Swal.fire(
              'Oop!!!',
              'Bạn nên chọn bất kì 1 ô nào đó',
              'question'
            )
        });
     });
</script>
@endsection
