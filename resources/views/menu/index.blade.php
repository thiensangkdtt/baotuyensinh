@extends('layouts.app', ['pageSlug' => 'menu'])
@section('content')
<div class="nav-wrap">
 <div class="btn-menu">
 	{!! Menu::render() !!}
@endsection

@push('scripts')
    {!! Menu::scripts() !!}
@endpush
