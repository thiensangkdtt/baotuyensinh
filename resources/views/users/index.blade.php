@extends('layouts.app', ['page' => __('User Management'), 'pageSlug' => 'users'])

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div style="background-color: #f7f2dd;" class="card ">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h4 style="color: black;" class="card-title">{{ __('Users') }}</h4>
                        </div>
                        <div  class="col-4 text-right">
                            <a style=" color: white; background-color: #4CAF50; padding: 12px 12px; border: 0px;" href="{{ route('user.create') }}">{{ __('Add user') }}
                        </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @include('alerts.success')

                    <div class="">
                        <table class="table tablesorter " id="">
                            <thead class=" text-primary">
                                <th style="color: black;" scope="col">{{ __('Name') }}</th>
                                <th style="color: black;"  scope="col">{{ __('Email') }}</th>
                                <th style="color: black;"  scope="col">{{ __('Creation Date') }}</th>
                                <th style="color: black;"  scope="col"></th>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td><p style="color: black;">{{ $user->name }}</p></td>
                                        <td>
                                            <a style="color: green;" href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                                        </td>
                                        <td><p style="color: black;">{{ $user->created_at->format('d/m/Y H:i') }}</p></td>
                                        <td class="text-right">
                                                <div class="dropdown">
                                                    <a style="background-color: #4caf50; padding: 5px 14px; border-radius: 14px; color: white;" class="btn-icon-only" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        @if (auth()->user()->id != $user->id)
                                                            <form action="{{ route('user.destroy', $user) }}" method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <a class="dropdown-item" href="{{ route('user.edit', $user) }}">{{ __('Edit') }}</a>
                                                                <button type="button" class="dropdown-item" onclick="confirm('{{ __("Are you sure you want to delete this user?") }}') ? this.parentElement.submit() : ''">
                                                                            {{ __('Delete') }}
                                                                </button>
                                                            </form>
                                                        @else
                                                            <a class="dropdown-item" href="{{ route('profile.edit') }}">{{ __('Edit') }}</a>
                                                        @endif
                                                    </div>
                                                </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                        {{ $users->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
