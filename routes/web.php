<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
		Route::get('icons', ['as' => 'pages.icons', 'uses' => 'PageController@icons']);
		Route::get('maps', ['as' => 'pages.maps', 'uses' => 'PageController@maps']);
		Route::get('notifications', ['as' => 'pages.notifications', 'uses' => 'PageController@notifications']);
		Route::get('rtl', ['as' => 'pages.rtl', 'uses' => 'PageController@rtl']);
		Route::get('tables', ['as' => 'pages.tables', 'uses' => 'PageController@tables']);
		Route::get('typography', ['as' => 'pages.typography', 'uses' => 'PageController@typography']);
		Route::get('upgrade', ['as' => 'pages.upgrade', 'uses' => 'PageController@upgrade']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Route::group(['prefix'=>'post'], function () {
	Route::group(['middleware' => 'auth'], function () {

	Route::get('index','PostController@index')->name('indexPost');

	Route::get('create','PostController@create')->name('createPost');

	Route::post('store','PostController@store')->name('storePost');

	Route::get('edit','PostController@edit')->name('editPost');

	Route::post('update','PostController@update')->name('updatePost');

	Route::post('destroy','PostController@destroy')->name('destroyPost');

	Route::get('search','PostController@search')->name('searchPost');

	Route::get('filter','PostController@filter')->name('filterPost');

	Route::post('activate','PostController@activate')->name('activatePost');

	Route::post('slug','PostController@slug')->name('slugPost');

	Route::get('search','PostController@search')->name('searchPost');

	Route::post('temp','PostController@temp')->name('tempPost');
	});
});

Route::group(['prefix'=>'banner'], function () {
	Route::group(['middleware' => 'auth'], function () {

	Route::get('index','BannerController@index')->name('indexBanner');

	Route::get('create','BannerController@create')->name('createBanner');

	Route::post('store','BannerController@store')->name('storeBanner');

	Route::get('edit','BannerController@edit')->name('editBanner');

	Route::post('update','BannerController@update')->name('updateBanner');

	Route::post('destroy','BannerController@destroy')->name('destroyBanner');

	Route::get('search','BannerController@search')->name('searchBanner');

	Route::get('filter','BannerController@filter')->name('filterBanner');

	Route::post('activate','BannerController@activate')->name('activateBanner');
	});
});
Route::group(['prefix'=>'categories'], function () {
	Route::group(['middleware' => 'auth'], function () {

	Route::get('index','CategoriesController@index')->name('indexCategories');

	Route::get('create','CategoriesController@create')->name('createCategories');

	Route::post('store','CategoriesController@store')->name('storeCategories');

	Route::get('edit','CategoriesController@edit')->name('editCategories');

	Route::post('update','CategoriesController@update')->name('updateCategories');

	Route::post('destroy','CategoriesController@destroy')->name('destroyCategories');

	Route::get('search','CategoriesController@search')->name('searchCategories');

	Route::get('filter','CategoriesController@filter')->name('filterCategories');

	Route::post('activate','CategoriesController@activate')->name('activateCategories');
	});
});

Route::group(['prefix'=>'menu'], function () {
	Route::group(['middleware' => 'auth'], function () {

	Route::get('index','MenuController@index')->name('indexMenu');

	// Route::get('create','CategoriesController@create')->name('createCategories');

	// Route::post('store','CategoriesController@store')->name('storeCategories');

	// Route::get('edit','CategoriesController@edit')->name('editCategories');

	// Route::post('update','CategoriesController@update')->name('updateCategories');

	// Route::post('destroy','CategoriesController@destroy')->name('destroyCategories');
	});
});

Route::group(['prefix'=>'elastic'], function () {
	Route::group(['middleware' => 'auth'], function () {

	Route::get('search','ElasticsearchController@search')->name('searchElasticsearchController');

	Route::get('init','ElasticsearchController@init')->name('initController');
	});
});